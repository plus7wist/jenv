package main

import (
	"encoding/json"
	"flag"
	"github.com/chonla/format"
	"os"
	"path"
	"strings"
)

type EnvRecord struct {
	Name  string
	Value string
}

func main() {
	const (
		formatBash     = "export %<name>s=\"%<value>s\"\n"
		formatFish     = "set -x %<name>s \"%<value>s\"\n"
		formatKeyValue = "%<name>s=%<value>s\n"
	)

	var (
		flagFormat     = flag.String("f", formatKeyValue, "format to display")
		flagFormatBash = flag.Bool("fbash", false, formatBash)
		flagFormatFish = flag.Bool("ffish", false, formatFish)
	)

	flag.Parse()

	switch true {
	case *flagFormatBash:
		*flagFormat = formatBash
	case *flagFormatFish:
		*flagFormat = formatFish
	default:
	}

	records, err := LoadRecordList()
	if err != nil {
		panic(err)
	}

	// envMap loads and maintains envvars.

	envMap := make(map[string]string)
	for _, keyValue := range os.Environ() {
		chunks := strings.SplitN(keyValue, "=", 2)
		envMap[chunks[0]] = chunks[1]
	}

	mapping := func(name string) (value string) {
		value, ok := envMap[name]
		if ok {
			return
		}
		value = os.Getenv(name)
		return
	}

	// After expand envvars, the later record will overwrite any former.

	resolved := make([]EnvRecord, 0)
	resolvedIndex := make(map[string]int)

	for _, record := range records {
		record.Value = os.Expand(record.Value, mapping)
		envMap[record.Name] = record.Value

		if i, ok := resolvedIndex[record.Name]; ok {
			resolved[i] = record
		} else {
			resolvedIndex[record.Name] = len(resolved)
			resolved = append(resolved, record)
		}
	}

	for _, record := range resolved {
		format.Printf(*flagFormat, map[string]interface{}{
			"name":  record.Name,
			"value": record.Value,
		})
	}
}

func LoadRecordList() (env []EnvRecord, err error) {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return
	}

	jenvPath := path.Join(homeDir, ".env.json")

	file, err := os.Open(jenvPath)
	if err != nil {
		return
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&env)
	if err != nil {
		return
	}

	return
}
